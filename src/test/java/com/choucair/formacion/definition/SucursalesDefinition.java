package com.choucair.formacion.definition;

import com.choucair.formacion.steps.SucursalesSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SucursalesDefinition {

	@Steps
	SucursalesSteps sucursalesSteps;
	
	
	@Given("^Ingresar al sitio Visitanos$")
	public void ingresar_al_sitio_Visitanos()  {
		sucursalesSteps.ingresoSucursales();
	}
	
	
	@Given("^verificar ingreso a sucursales$")
	public void verificar_ingreso_a_sucursales()  {

	}


	@When("^Buscar oficina cercana ciudad \"([^\"]*)\"$")
	public void buscar_oficina_cercana_ciudad(String Ciudad) throws InterruptedException  {
		
		sucursalesSteps.buscarOficinas(Ciudad);
	}

	
	

	@Then("^Ubicarla en el mapa$")
	public void ubicarla_en_el_mapa()  {
		sucursalesSteps.buscarUbicacionEnMapa();
	}
}
