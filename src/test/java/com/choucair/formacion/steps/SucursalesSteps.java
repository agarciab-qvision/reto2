package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.SucursalesPage;

import net.thucydides.core.annotations.Step;



public class SucursalesSteps {

	SucursalesPage sucursalesPage;
	
	@Step
	public void ingresoSucursales(){
		sucursalesPage.open();
		sucursalesPage.ingresoSitioSucursales();
		sucursalesPage.verificaringresoSitioSucursales();
	}

	@Step
	public void buscarOficinas(String strCiudad) throws InterruptedException {
		
		sucursalesPage.IngresarCiudad(strCiudad);

		sucursalesPage.SeleccionarOficina();
	

		
	}

	@Step
	public void buscarUbicacionEnMapa() {
		sucursalesPage.realizarUbicacionEnMapa();
		sucursalesPage.confirmarUbicacionDelSitio();
		
	}


}
