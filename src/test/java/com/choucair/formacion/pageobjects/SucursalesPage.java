package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.junit.Assert.assertThat;

import static org.hamcrest.Matchers.containsString;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")

public class SucursalesPage extends PageObject {

	
	// Acceso Sucursales - Visitanos
	
	@FindBy(xpath="//*[@id=\'footer-content\']/div[1]/div/div/div[4]/div/a/img")
	public WebElementFacade btnVisitanos;

	public void ingresoSitioSucursales() {
		btnVisitanos.click();
		
	}


	// Verificar acceso - Sucursales
	
	@FindBy(xpath="//*[@id=\'mapaHtml\']/div[1]/h1")
	public WebElementFacade lblVisitanos;
	
	
	public void verificaringresoSitioSucursales() {

		String lblVerificacion = "Visítanos";
		String strMensaje = lblVisitanos.getText();
		assertThat(strMensaje, containsString(lblVerificacion));

		
	}

	//Buscar oficina
	
	@FindBy(xpath="//*[@id=\'srch-term\']")
	public WebElementFacade txtOficina;
	
	@FindBy(xpath="//*[@id=\'tab1\']/div[1]/div[1]/div/button/input")
	public WebElementFacade btnBuscar;	
	
	
	
	public void temporizador(int intervaloEspera) throws InterruptedException {
		Thread.sleep(intervaloEspera * 1000);
		
	}

	public void IngresarCiudad(String strCiudad) throws InterruptedException {
		txtOficina.sendKeys(strCiudad);
		btnBuscar.click();
		temporizador(3);
	}

	
	@FindBy(xpath="//*[@id=\'tab1\']/div[1]/div[6]/div[1]/div/div[1]/button")
	
	public WebElementFacade btnSeleccionarOficina;	
	
	
	public void SeleccionarOficina() {
		btnSeleccionarOficina.click();
	}

	
	@FindBy(xpath="//*[@id=\'mapa\']/div/div/div[1]/div[4]/div[4]/div[1]/div/h3")
	public WebElementFacade popupUbicacionEnMapa;	
	
	
	
	public void realizarUbicacionEnMapa() {
		String lblVerificacion = "CENTRO DE PAGOS BELLO";
		String strMensaje = popupUbicacionEnMapa.getText();
		assertThat(strMensaje, containsString(lblVerificacion)); 
		
	}
	
	
	@FindBy(xpath="//*[@id=\'tab1\']/div[1]/div[6]/div[1]/div/div[2]/h3[1]")
	public WebElementFacade lblPrimerSitio;
	
		public void confirmarUbicacionDelSitio() {
		String lblVerificacion = "CENTRO DE PAGOS BELLO";
		String strMensaje = lblPrimerSitio.getText();
		assertThat(strMensaje, containsString(lblVerificacion)); 
	}
	
}


